#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e

stage=9

# opts
case=near-far
nnet_dir=$1
mdl=`basename $nnet_dir`
enroll_name=$2   # e.g., 1m
test_name=$3     # e.g., 3m
task=${enroll_name}_to_${test_name}

dev_enroll_data=$nnet_dir/$case/xvectors_${enroll_name}_dev      # dev in enroll condition
dev_test_data=$nnet_dir/$case/xvectors_${test_name}_dev          # dev in test condition
enroll_data=$nnet_dir/$case/xvectors_${enroll_name}_enroll       # enroll data
test_data=$nnet_dir/$case/xvectors_${test_name}_test             # test data

flow=linear
num_blocks=1    # one linear layer
num_inputs=512  # dim of x-vector
batch_size=512
epochs=601
lr=0.00001
device=1

ckpt_save_interval=20
model=${mdl}_${case}_${task}_${flow}_ni${num_inputs}_nb${num_blocks}_bs${batch_size}_lr${lr}
ckpt_dir=ckpt/$model
log_dir=$ckpt_dir/log

echo $model

### For stage 9 and stage 10, they are task-independent, so only need to execute one time.

# compute PLDA model (also refer to stage 3)
if [ $stage -le 9 ]; then
  if [ ! -f $dev_enroll_data/plda_ ]; then
     ivector-copy-plda --binary=false $dev_enroll_data/plda $dev_enroll_data/plda_
  fi

  if [ ! -f $dev_test_data/plda_ ]; then
     ivector-copy-plda --binary=false $dev_test_data/plda $dev_test_data/plda_
  fi
fi


# # kaldi to npz (also refer to stage 4)
# if [ $stage -le 10 ]; then
#   for dis in 1m 3m 5m; do
#     for sub in dev enroll test; do
#       python -u local/kaldi2npz.py \
#              --is-eval \
#              --src-file $nnet_dir/$case/xvectors_${dis}_${sub}/xvector.scp \
#              --dest-file $nnet_dir/$case/xvectors_${dis}_${sub}/xvector.npz \
#              --utt2spk-file $nnet_dir/$case/xvectors_${dis}_${sub}/utt2spk
#     done
#   done
# fi


# Make PLDA for enroll, dev and test data
if [ $stage -le 11 ]; then
  for sub in $dev_enroll_data $dev_test_data $enroll_data $test_data; do
    echo $sub
    python -u local/transform_vector.py \
        --source-npz $sub/xvector.npz \
        --dest-npz $sub/xvector_plda.npz \
        --global-mean $dev_enroll_data/mean.vec \
        --plda $dev_enroll_data/plda_ \
        --plda-dim $num_inputs
  done
fi


# MLE training
if [ $stage -le 12 ]; then
  # MLE configs
  mkdir -p $log_dir
  echo "Start ..."
  # This may be a bug in pyTorch... you must assign GPU device before the main.py.
  CUDA_VISIBLE_DEVICES=$device python -u dnf/main.py \
         --dataset-name $task \
         --ckpt-dir $ckpt_dir \
         --raw-data-npz $dev_enroll_data/xvector_plda.npz \
         --mdl-plda $dev_enroll_data/plda_ \
         --dev-data-npz $dev_test_data/xvector_plda.npz \
         --num-blocks $num_blocks \
         --num-inputs $num_inputs \
         --batch-size $batch_size \
         --epochs $epochs \
         --lr $lr \
         --device 0 \
         --ckpt-save-interval $ckpt_save_interval \
         --ckpt-dir $ckpt_dir \
         --log-dir $log_dir
  echo "End ..."
fi


# MLE inference
if [ $stage -le 13 ]; then
  echo $model
  echo "Start to infer data from x space to z space and store to numpy npz"
  for ((infer_epoch=0; infer_epoch<${epochs}; infer_epoch=infer_epoch+20)); do
    echo $infer_epoch
    # This may be a bug in pyTorch... you must assign GPU device before the main.py.
    CUDA_VISIBLE_DEVICES=$device python -u dnf/main.py \
               --dataset-name $task \
               --eval \
               --infer-epoch $infer_epoch \
               --ckpt-dir $ckpt_dir \
               --mdl-plda $dev_enroll_data/plda_ \
               --test-data-npz $test_data/xvector_plda.npz \
               --num-blocks $num_blocks \
               --num-inputs $num_inputs \
               --batch-size 50000 \
               --device 0 \
               --npz-dir $test_data/xvector_mle_${infer_epoch}.npz
  done
fi


# NL scoring
if [ $stage -le 14 ]; then
  echo baseline
  python -u local/score/nl_score_by_trials.py \
         --enroll-npz $enroll_data/xvector_plda.npz \
         --enroll-num-utts $enroll_data/num_utts.ark \
         --test-npz $test_data/xvector_plda.npz \
         --trials data/$case/$test_name/test/trials \
         --plda $dev_enroll_data/plda_ \
         --plda-dim $num_inputs
fi


# DAT scoring
if [ $stage -le 15 ]; then
  for ((infer_epoch=0; infer_epoch<${epochs}; infer_epoch=infer_epoch+20)); do
  {
    echo $infer_epoch
    python -u local/score/nl_score_by_trials.py \
           --enroll-npz $enroll_data/xvector_plda.npz \
           --enroll-num-utts $enroll_data/num_utts.ark \
           --test-npz $test_data/xvector_mle_${infer_epoch}.npz \
           --trials data/$case/$test_name/test/trials \
           --plda $dev_enroll_data/plda_ \
           --plda-dim $num_inputs
  }&
  done
  wait;
fi


# Make PLDA for test data
if [ $stage -le 16 ]; then
  for sub in $test_data; do
    echo $sub
    python -u local/transform_vector.py \
        --source-npz $sub/xvector.npz \
        --dest-npz $sub/xvector_plda_test.npz \
        --global-mean $dev_test_data/mean.vec \
        --plda $dev_test_data/plda_ \
        --plda-dim $num_inputs
  done
fi


# CT scoring
if [ $stage -le 17 ]; then
  for ((infer_epoch=0; infer_epoch<${epochs}; infer_epoch=infer_epoch+20)); do
  {
    echo $infer_epoch
    python -u local/score/nl_score_by_trials_test.py \
           --enroll-npz $enroll_data/xvector_plda.npz \
           --enroll-num-utts $enroll_data/num_utts.ark \
           --test-mle-npz $test_data/xvector_mle_${infer_epoch}.npz \
           --test-raw-npz $test_data/xvector_plda_test.npz \
           --trials data/$case/$test_name/test/trials \
           --plda-enroll $dev_enroll_data/plda_ \
           --plda-test $dev_test_data/plda_ \
           --plda-dim $num_inputs
  }&
  done
  wait;
fi

