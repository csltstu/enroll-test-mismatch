#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e
fbankdir=`pwd`/_fbank
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad

stage=1

case=aging
nnet_dir=$1
checkpoint='last'


# Make MFCCs and compute the energy-based VAD for each dataset
if [ $stage -le 1 ]; then
  for aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
    for sub in enroll test; do
      echo $aging/$sub
      steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 10 --cmd "$cmd" \
        data/$case/$aging/$sub exp/make_mfcc $mfccdir/$aging
      utils/fix_data_dir.sh data/$case/$aging/$sub

      sid/compute_vad_decision.sh --vad-config conf/vad.conf --nj 10 --cmd "$cmd" \
        data/$case/$aging/$sub exp/make_vad $vaddir/$aging
      utils/fix_data_dir.sh data/$case/$aging/$sub
      rm -r data/$case/$aging/$sub/feats.scp
      rm -r $mfccdir

      steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 10 --cmd "$cmd" \
        data/$case/$aging/$sub exp/make_fbank $fbankdir/$aging
      utils/fix_data_dir.sh data/$case/$aging/$sub
    done
  done
fi


# Extract the embeddings
if [ $stage -le 2 ]; then
  for aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
    for sub in enroll test; do
      echo $aging/$sub
      nnet/run_extract_embeddings.sh --cmd "$cmd" --nj 10 \
        --use-gpu false --checkpoint $checkpoint --stage 0 \
        --chunk-size 10000 --normalize false --node "tdnn6_dense" \
        $nnet_dir data/$case/$aging/$sub $nnet_dir/$case/xvectors_${aging}_${sub}
      cp data/$case/$aging/$sub/{spk2utt,utt2spk} $nnet_dir/$case/xvectors_${aging}_${sub}/
    done
  done
fi


# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 3 ]; then
  for enroll_aging in 1st 2nd 3rd; do
    for test_aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
      echo $enroll_aging to $test_aging

      local/score/plda_scoring.sh --normalize-length true --simple-length-norm true \
          xvector.scp \
          $nnet_dir/$case/xvectors_${enroll_aging}_enroll \
          $nnet_dir/$case/xvectors_${enroll_aging}_enroll \
          $nnet_dir/$case/xvectors_${test_aging}_test \
          data/$case/$test_aging/test/trials \
          scores/$case/${enroll_aging}-${test_aging}

    done
  done
fi


# kaldi to npz
if [ $stage -le 4 ]; then
  for aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
    for sub in enroll test; do
      python -u local/kaldi2npz.py \
             --is-eval \
             --src-file $nnet_dir/$case/xvectors_${aging}_${sub}/xvector.scp \
             --dest-file $nnet_dir/$case/xvectors_${aging}_${sub}/xvector.npz \
             --utt2spk-file $nnet_dir/$case/xvectors_${aging}_${sub}/utt2spk
    done
  done
fi


# Baseline: NL scoring.
if [ $stage -le 5 ]; then
  echo "Baseline: NL scoring."
  for dim in 56 512; do
    echo $dim
    for enroll_aging in 1st 2nd 3rd; do
      for test_aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
        echo $enroll_aging to $test_aging
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ ]; then
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_aging}-${test_aging}
        python -u local/score/plda_score_by_trials.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_aging}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_aging}_enroll/num_utts.ark \
               --test-npz $nnet_dir/$case/xvectors_${test_aging}_test/xvector.npz \
               --trials data/$case/$test_aging/test/trials \
               --score scores/$case/$dim/${enroll_aging}-${test_aging}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_aging}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi


# Special Case 1: x' = x + b.
if [ $stage -le 6 ]; then
  echo "Special Case 1: x' = x + b."
  for dim in 56 512; do
    echo $dim
    for enroll_aging in 1st 2nd 3rd; do
      for test_aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
        echo $enroll_aging to $test_aging
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_aging}-${test_aging}
        python -u local/score/plda_score_by_trials_bias.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_aging}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_aging}_enroll/num_utts.ark \
               --test-npz $nnet_dir/$case/xvectors_${test_aging}_test/xvector.npz \
               --trials data/$case/$test_aging/test/trials \
               --score scores/$case/$dim/${enroll_aging}-${test_aging}/plda_scores \
               --global-mean-enroll $nnet_dir/$case/xvectors_${enroll_aging}_enroll/mean.vec \
               --global-mean-test $nnet_dir/$case/xvectors_${test_aging}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi


# Special Case 2: scenarios sw'.
if [ $stage -le 7 ]; then
  echo "Special Case 2: scenarios sw'."
  for dim in 56 512; do
    echo $dim
    for enroll_aging in 1st 2nd 3rd; do
      for test_aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
        echo $enroll_aging to $test_aging
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_aging}-${test_aging}
        python -u local/score/plda_score_by_trials_sw.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_aging}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_aging}_enroll/num_utts.ark \
               --dev-npz $nnet_dir/$case/xvectors_${test_aging}_enroll/xvector.npz \
               --test-npz $nnet_dir/$case/xvectors_${test_aging}_test/xvector.npz \
               --trials data/$case/$test_aging/test/trials \
               --score scores/$case/$dim/${enroll_aging}-${test_aging}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_aging}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi


# Special Case 2: spk-dependent sw'.
if [ $stage -le 8 ]; then
  echo "Special Case 2: spk-dependent sw'."
  for dim in 56 512; do
    echo $dim
    for enroll_aging in 1st 2nd 3rd; do
      for test_aging in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
        echo $enroll_aging to $test_aging
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_aging}-${test_aging}
        python -u local/score/plda_score_by_trials_sw_spk.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_aging}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_aging}_enroll/num_utts.ark \
               --dev-npz $nnet_dir/$case/xvectors_${test_aging}_enroll/xvector.npz \
               --test-npz $nnet_dir/$case/xvectors_${test_aging}_test/xvector.npz \
               --trials data/$case/$test_aging/test/trials \
               --score scores/$case/$dim/${enroll_aging}-${test_aging}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_aging}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_aging}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi

