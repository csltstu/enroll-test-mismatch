#!/bin/bash

## start from stage 11

mkdir -p log/aging
for i in 1st 2nd 3rd; do
  for j in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
    echo $i $j
    sh run_aging_gf.sh archive/vox-sp-sm $i $j > log/aging/${i}_${j}.log
  done
done


mkdir -p log/channel
for i in Android iOS Mic; do
  for j in Android iOS Mic; do
    echo $i $j
    sh run_channel_gf.sh archive/vox-sp-sm $i $j > log/channel/${i}_${j}.log
  done
done


mkdir -p log/near-far
for i in 1m 3m 5m; do
  for j in 1m 3m 5m; do
    echo $i $j
    sh run_near-far_gf.sh archive/vox-sp-sm $i $j > log/near-far/${i}_${j}.log
  done
done

