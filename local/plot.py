import os, sys
import numpy as np
from matplotlib import pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt('1st.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Aging (1st)')
    plt.plot(data[0, :], label='1st-1st')
    plt.plot(data[1, :], label='1st-2nd')
    plt.plot(data[2, :], label='1st-3rd')
    plt.plot(data[3, :], label='1st-4th')
    plt.plot(data[4, :], label='1st-5th')
    plt.plot(data[5, :], label='1st-6th')
    plt.plot(data[6, :], label='1st-7th')
    plt.plot(data[7, :], label='1st-8th')
    plt.legend()
    plt.savefig('1st.png')


    data = np.loadtxt('3rd.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Aging (3nd)')
    plt.plot(data[0, :], label='3rd-1st')
    plt.plot(data[1, :], label='3rd-2nd')
    plt.plot(data[2, :], label='3rd-3rd')
    plt.plot(data[3, :], label='3rd-4th')
    plt.plot(data[4, :], label='3rd-5th')
    plt.plot(data[5, :], label='3rd-6th')
    plt.plot(data[6, :], label='3rd-7th')
    plt.plot(data[7, :], label='3rd-8th')
    plt.legend()
    plt.savefig('3rd.png')

    ###

    data = np.loadtxt('Android.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Cross-channel (Android)')
    plt.plot(data[0, :], label='Android-Android')
    plt.plot(data[1, :], label='Android-Mic')
    plt.plot(data[2, :], label='Android-iOS')
    plt.legend()
    plt.savefig('Android.png')


    data = np.loadtxt('Mic.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Cross-channel (Mic)')
    plt.plot(data[0, :], label='Mic-Android')
    plt.plot(data[1, :], label='Mic-Mic')
    plt.plot(data[2, :], label='Mic-iOS')
    plt.legend()
    plt.savefig('Mic.png')

    ###

    data = np.loadtxt('1m.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Near-far (1m)')
    plt.plot(data[0, :], label='1m-1m')
    plt.plot(data[1, :], label='1m-3m')
    plt.plot(data[2, :], label='1m-5m')
    plt.legend()
    plt.savefig('1m.png')


    data = np.loadtxt('5m.log', dtype=np.float)
    print(np.shape(data))
    plt.figure()
    plt.title('Near-far (5m)')
    plt.plot(data[0, :], label='5m-1m')
    plt.plot(data[1, :], label='5m-3m')
    plt.plot(data[2, :], label='5m-5m')
    plt.legend()
    plt.savefig('5m.png')

