#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e
fbankdir=`pwd`/_fbank
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad

stage=0

#nnet_dir=archive/vox-sp-sm
nnet_dir=archive/vox-rn-sp-aam


# aging
if [ $stage -le 0 ]; then
  case=aging
  for i in 1st 2nd 3rd; do
    for j in 1st 2nd 3rd 4th 5th 6th 7th 8th; do
      if [ ! -f $nnet_dir/$case/xvectors_${i}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${i}_enroll/plda $nnet_dir/$case/xvectors_${i}_enroll/plda_
      fi

      if [ ! -f $nnet_dir/$case/xvectors_${j}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${j}_enroll/plda $nnet_dir/$case/xvectors_${j}_enroll/plda_
      fi

      python -u local/compute_stats.py \
             --plda-A $nnet_dir/$case/xvectors_${i}_enroll/plda_ \
             --plda-B $nnet_dir/$case/xvectors_${j}_enroll/plda_ \
             --metric cosine \
             --saver ${i}.log
    done
  done
fi


# device
if [ $stage -le 1 ]; then
  case=channel
  for i in Android Mic iOS; do
    for j in Android Mic iOS; do
      if [ ! -f $nnet_dir/$case/xvectors_${i}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${i}_enroll/plda $nnet_dir/$case/xvectors_${i}_enroll/plda_
      fi

      if [ ! -f $nnet_dir/$case/xvectors_${j}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${j}_enroll/plda $nnet_dir/$case/xvectors_${j}_enroll/plda_
      fi

      python -u local/compute_stats.py \
             --plda-A $nnet_dir/$case/xvectors_${i}_enroll/plda_ \
             --plda-B $nnet_dir/$case/xvectors_${j}_enroll/plda_ \
             --metric cosine \
             --saver ${i}.log
    done
  done
fi


# near-far
if [ $stage -le 2 ]; then
  case=near-far
  for i in 1m 3m 5m; do
    for j in 1m 3m 5m; do
      if [ ! -f $nnet_dir/$case/xvectors_${i}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${i}_enroll/plda $nnet_dir/$case/xvectors_${i}_enroll/plda_
      fi

      if [ ! -f $nnet_dir/$case/xvectors_${j}_enroll/plda_ ]; then
        ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${j}_enroll/plda $nnet_dir/$case/xvectors_${j}_enroll/plda_
      fi

      python -u local/compute_stats.py \
             --plda-A $nnet_dir/$case/xvectors_${i}_enroll/plda_ \
             --plda-B $nnet_dir/$case/xvectors_${j}_enroll/plda_ \
             --metric cosine \
             --saver ${i}.log
    done
  done
fi

