import os
import sys

'''
Create test trials
'''
def makeTrial(enr, test, trl):
    f = open(enr, 'r')
    lines_enr = f.readlines()
    f.close()
    f = open(test, 'r')
    lines_test = f.readlines()
    f.close()

    f = open(trl, 'w')
    for enroll in lines_enr:
        spk = enroll.strip().split()[0]
        for test in lines_test:
            utt = test.strip().split()[0]
            if spk == utt.split('-')[0]:
                f.write(spk + ' ' + utt + ' target' + '\n')
            else:
                f.write(spk + ' ' + utt + ' nontarget' + '\n') 
    f.close()

if __name__ =="__main__":
    if len(sys.argv) != 4:
        sys.stderr.write('\
        Usage: ./make_trial.py enroll/spk2utt verify/spk2utt test.trl \
        \n')
    else:
        fin_enr = sys.argv[1]
        fin_ver = sys.argv[2]
        fout_trl = sys.argv[3]
        makeTrial(fin_enr, fin_ver, fout_trl)
