import os
import numpy as np
import argparse
import math

def LoadGlobalMean(f_global_mean):
    f = open(f_global_mean, 'r')
    line = f.readline()
    f.close()

    part = line.strip().split()
    global_mean = np.array(part[1:-1], dtype=np.float)
    return global_mean


def LoadPLDA(mdl_plda):
    f = open(mdl_plda, 'r')
    lines = f.readlines()
    f.close()

    dim = len(lines[2].strip().split())
    # print(dim)

    plda_W = []
    for line in lines:
        part = line.strip().split()
        if len(part) == (dim + 3):
            plda_b = np.array(part[2:-1], dtype=np.float)
        elif len(part) == dim:
            plda_W.append(np.array(part, dtype=np.float))
        elif len(part) == (dim + 1):
            plda_W.append(np.array(part[:-1], dtype=np.float))
        elif len(part) == (dim + 2):
            plda_SB = np.array(part[1:-1], dtype=np.float)
        else:
            continue
    return plda_b, np.array(plda_W), plda_SB


def GetNormalizationFactor(transformed_vector, num_utts, plda_SB):
  assert(num_utts > 0)
  dim = len(transformed_vector)
  inv_covar = 1.0 / (1.0 / num_utts + plda_SB)
  dot_prod = np.dot(inv_covar, transformed_vector ** 2)
  return math.sqrt(dim / dot_prod)


def TransformVector(vector, num_utts, plda_W, plda_b, plda_SB, plda_dim, simple_length_norm=False, normalize_length=False):
    dim = len(vector)
    normalization_factor = 0.0
    transformed_vector = np.dot(vector - plda_b, plda_W)[:plda_dim]
    if simple_length_norm:
        normalization_factor = math.sqrt(dim) / np.linalg.norm(transformed_vector)
    else:
        normalization_factor = GetNormalizationFactor(transformed_vector, num_utts, plda_SB[:plda_dim])
    if normalize_length:
        transformed_vector = transformed_vector * normalization_factor
    return transformed_vector


def PLDA_transform(source_npz, dest_npz, global_mean, plda_W, plda_b, plda_SB, plda_dim):
    assert os.path.exists(source_npz) == True

    vectors = np.load(source_npz)['vectors']
    spkers = np.load(source_npz)['spker_label']
    utters = np.load(source_npz)['utt_label']
    
    vectors = vectors - global_mean

    # transform vectors
    trans_vecs = [] 
    for idx in range(len(utters)):
        vec = vectors[idx]
        trans_vec = TransformVector(vec, 1, plda_W, plda_b, plda_SB, plda_dim)
        trans_vecs.append(trans_vec)
    trans_vecs = np.array(trans_vecs)

    # save transformed vectors
    np.savez(dest_npz, vectors=trans_vecs, spker_label=spkers, utt_label=utters)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--source-npz', default='xvector.npz', help='npz file of raw input vector')
    parser.add_argument(
        '--dest-npz', default='xvector.npz', help='npz file of transformed vector')
    parser.add_argument(
        '--global-mean', default='mean.vec', help='global mean')
    parser.add_argument(
        '--plda', default='plda_', help='file of plda model in Kaldi')
    parser.add_argument(
        '--plda-dim', type=int, default=150, help='dim of PLDA')

    args = parser.parse_args()

    global_mean = LoadGlobalMean(args.global_mean)

    # Load PLDA
    plda_b, plda_W, plda_SB = LoadPLDA(args.plda)
    plda_W = plda_W.T # W transpose

    # transform vector
    PLDA_transform(args.source_npz, args.dest_npz, global_mean, plda_W, plda_b, plda_SB, args.plda_dim)

